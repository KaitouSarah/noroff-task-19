# Noroff Task 19

Task 19: Post Graduate Manager Part 1 

 Write a program which allows a university post grad administrator to assign Supervisors (Professors) to Students to supervise them during their postgrad research degrees 

 Create a basic UI for it but do not wire up any events yet (name all your components correctly) 

 Use a code first workflow to create the initial class for supervisorand the corresponding database using entity framework (DBContext)

  Do not create the Student class yet! Only the Supervisor Class with Id and Nam

_________________________________

Part 2 - Task 20

 Add the Student class to you PGM solution (task 19) with a Supervisor object inside it 

 Add the migrations to update the database structure 

 Insert some sample data into the supervisor table using a migration 

 Populate a ListBox or ComboBoxwith the Supervisors Names from the database

 Add some textboxes to allow for adding students 

 Bind the gridview to the student table 

 Usethe gridview to allow for Deleting students (Hint DGV allows you to use the cell values)

_________________________________

Part 2 - Task 21
 Add a button into your PGManagerproject to allow for the current DBSetof Supervisors to be serialized into JSON. 

 You can either write the JSON to a text file within your debug folder and submit the text file or add a component to your UI to display the JSON text