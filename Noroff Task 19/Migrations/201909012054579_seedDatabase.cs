namespace Noroff_Task_19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDatabase : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors (Name) VALUES ('Thomasu')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Dean')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Greg')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name = 'Thomasu'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Dean'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Greg'");
        }
    }
}
