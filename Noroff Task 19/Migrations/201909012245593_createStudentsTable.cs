namespace Noroff_Task_19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class createStudentsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.Students",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   Name = c.String(),
                   SupervisorId = c.Int(nullable: false),
               })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisors", t => t.SupervisorId, cascadeDelete: true);
        }

        public override void Down()
        {
            DropTable("dbo.Students");
        }
    }
}
