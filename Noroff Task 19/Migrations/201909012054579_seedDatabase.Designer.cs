// <auto-generated />
namespace Noroff_Task_19.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class seedDatabase : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(seedDatabase));
        
        string IMigrationMetadata.Id
        {
            get { return "201909012054579_seedDatabase"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
