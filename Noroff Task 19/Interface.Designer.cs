﻿namespace Noroff_Task_19
{
    partial class Interface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxAddStudent = new System.Windows.Forms.GroupBox();
            this.textBoxAddStudent = new System.Windows.Forms.TextBox();
            this.labelSelectStudent = new System.Windows.Forms.Label();
            this.labelSelectSupervisor = new System.Windows.Forms.Label();
            this.buttonAddStudent = new System.Windows.Forms.Button();
            this.comboBoxSupervisor = new System.Windows.Forms.ComboBox();
            this.buttonDeleteStudent = new System.Windows.Forms.Button();
            this.buttonSupervisorsAsJSON = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxAddStudent.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(347, 50);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(413, 246);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // groupBoxAddStudent
            // 
            this.groupBoxAddStudent.Controls.Add(this.textBoxAddStudent);
            this.groupBoxAddStudent.Controls.Add(this.labelSelectStudent);
            this.groupBoxAddStudent.Controls.Add(this.labelSelectSupervisor);
            this.groupBoxAddStudent.Controls.Add(this.buttonAddStudent);
            this.groupBoxAddStudent.Controls.Add(this.comboBoxSupervisor);
            this.groupBoxAddStudent.Location = new System.Drawing.Point(31, 48);
            this.groupBoxAddStudent.Name = "groupBoxAddStudent";
            this.groupBoxAddStudent.Size = new System.Drawing.Size(243, 248);
            this.groupBoxAddStudent.TabIndex = 6;
            this.groupBoxAddStudent.TabStop = false;
            this.groupBoxAddStudent.Text = "Add student";
            // 
            // textBoxAddStudent
            // 
            this.textBoxAddStudent.Location = new System.Drawing.Point(7, 57);
            this.textBoxAddStudent.Name = "textBoxAddStudent";
            this.textBoxAddStudent.Size = new System.Drawing.Size(165, 20);
            this.textBoxAddStudent.TabIndex = 10;
            // 
            // labelSelectStudent
            // 
            this.labelSelectStudent.AutoSize = true;
            this.labelSelectStudent.Location = new System.Drawing.Point(6, 40);
            this.labelSelectStudent.Name = "labelSelectStudent";
            this.labelSelectStudent.Size = new System.Drawing.Size(99, 13);
            this.labelSelectStudent.TabIndex = 9;
            this.labelSelectStudent.Text = "Enter student name";
            // 
            // labelSelectSupervisor
            // 
            this.labelSelectSupervisor.AutoSize = true;
            this.labelSelectSupervisor.Location = new System.Drawing.Point(4, 115);
            this.labelSelectSupervisor.Name = "labelSelectSupervisor";
            this.labelSelectSupervisor.Size = new System.Drawing.Size(97, 13);
            this.labelSelectSupervisor.TabIndex = 8;
            this.labelSelectSupervisor.Text = "Select a supervisor";
            // 
            // buttonAddStudent
            // 
            this.buttonAddStudent.Location = new System.Drawing.Point(7, 190);
            this.buttonAddStudent.Name = "buttonAddStudent";
            this.buttonAddStudent.Size = new System.Drawing.Size(75, 23);
            this.buttonAddStudent.TabIndex = 7;
            this.buttonAddStudent.Text = "Add student";
            this.buttonAddStudent.UseVisualStyleBackColor = true;
            this.buttonAddStudent.Click += new System.EventHandler(this.ButtonAddStudent_Click);
            // 
            // comboBoxSupervisor
            // 
            this.comboBoxSupervisor.FormattingEnabled = true;
            this.comboBoxSupervisor.Location = new System.Drawing.Point(7, 131);
            this.comboBoxSupervisor.Name = "comboBoxSupervisor";
            this.comboBoxSupervisor.Size = new System.Drawing.Size(165, 21);
            this.comboBoxSupervisor.TabIndex = 5;
            // 
            // buttonDeleteStudent
            // 
            this.buttonDeleteStudent.Location = new System.Drawing.Point(684, 329);
            this.buttonDeleteStudent.Name = "buttonDeleteStudent";
            this.buttonDeleteStudent.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteStudent.TabIndex = 7;
            this.buttonDeleteStudent.Text = "Delete student";
            this.buttonDeleteStudent.UseVisualStyleBackColor = true;
            this.buttonDeleteStudent.Click += new System.EventHandler(this.ButtonDeleteStudent_Click);
            // 
            // buttonSupervisorsAsJSON
            // 
            this.buttonSupervisorsAsJSON.Location = new System.Drawing.Point(13, 402);
            this.buttonSupervisorsAsJSON.Name = "buttonSupervisorsAsJSON";
            this.buttonSupervisorsAsJSON.Size = new System.Drawing.Size(161, 36);
            this.buttonSupervisorsAsJSON.TabIndex = 8;
            this.buttonSupervisorsAsJSON.Text = "Show supervisors as JSON";
            this.buttonSupervisorsAsJSON.UseVisualStyleBackColor = true;
            this.buttonSupervisorsAsJSON.Click += new System.EventHandler(this.ButtonSupervisorsAsJSON_Click);
            // 
            // Interface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSupervisorsAsJSON);
            this.Controls.Add(this.buttonDeleteStudent);
            this.Controls.Add(this.groupBoxAddStudent);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Interface";
            this.Text = "SupervisorToStudent";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxAddStudent.ResumeLayout(false);
            this.groupBoxAddStudent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBoxAddStudent;
        private System.Windows.Forms.Label labelSelectStudent;
        private System.Windows.Forms.Label labelSelectSupervisor;
        private System.Windows.Forms.Button buttonAddStudent;
        private System.Windows.Forms.ComboBox comboBoxSupervisor;
        private System.Windows.Forms.TextBox textBoxAddStudent;
        private System.Windows.Forms.Button buttonDeleteStudent;
        private System.Windows.Forms.Button buttonSupervisorsAsJSON;
    }
}

