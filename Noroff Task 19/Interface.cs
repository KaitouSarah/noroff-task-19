﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Noroff_Task_19.Model;

namespace Noroff_Task_19
{
    public partial class Interface : Form
    {
        private pgmDBContext pgmDBContext;

        public Interface()
        {
            InitializeComponent();
            pgmDBContext = new pgmDBContext();

            //Add stuf to combobox
            List<Supervisor> supervisors = pgmDBContext.Supervisors.ToList();
            foreach (Supervisor supervisor in supervisors)
            {
                comboBoxSupervisor.Items.Add(supervisor.Name);
            }

            //Add stuff to grid view
            RefreshDataGridView1();



        }


        private void ButtonConfirm_Click(object sender, EventArgs e)
        {

        }

        private void ButtonAddStudent_Click(object sender, EventArgs e)
        {
            string studentName = textBoxAddStudent.Text;
            string supervisorName = comboBoxSupervisor.Text;
            AddStudentToDatabase(studentName, supervisorName);

        }

        private void AddStudentToDatabase(string studentName, string supervisorName)
        {
            Supervisor supervisor = (Supervisor) pgmDBContext.Supervisors.Where(b => b.Name == supervisorName).FirstOrDefault();
            Student student = new Student(studentName, supervisor);
            pgmDBContext.Students.Add(student);
            pgmDBContext.SaveChanges();
            RefreshDataGridView1();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void RefreshDataGridView1()
        {
            BindingSource myBindingSource = new BindingSource();

            dataGridView1.DataSource = myBindingSource;

            myBindingSource.DataSource = pgmDBContext.Students.ToList();

            dataGridView1.Refresh();
        }

        private void ButtonDeleteStudent_Click(object sender, EventArgs e)
        {
            int id = (int)dataGridView1.SelectedCells[0].OwningRow.Cells["Id"].Value;

            Student student = pgmDBContext.Students.Find(id);
            pgmDBContext.Students.Remove(student);
            pgmDBContext.SaveChanges();

            RefreshDataGridView1();
        }

        private void ButtonSupervisorsAsJSON_Click(object sender, EventArgs e)
        {
            string JSONStringOfSupervisors = "";

            foreach(Supervisor supervisor in pgmDBContext.Supervisors)
            {
                JSONStringOfSupervisors += $"{supervisor.SerializeSupervisor()},\n";
            }

            MessageBox.Show(JSONStringOfSupervisors);
        }
    }
}
