﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff_Task_19.Model
{
    class Supervisor
    {
        public Supervisor() { }

        public Supervisor(string name)
        {
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public string SerializeSupervisor()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
