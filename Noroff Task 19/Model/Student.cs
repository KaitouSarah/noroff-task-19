﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff_Task_19.Model
{
    class Student
    {

        public Student() { }

        public Student(string name, Supervisor supervisor)
        {
            Name = name;
            Supervisor = supervisor;
            SupervisorId = supervisor.Id;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public int SupervisorId { get; set; }
        public Supervisor Supervisor { get; set; }

    }
}
